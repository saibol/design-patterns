package com.sai.patterns.abstractfactory;

public class AbstractFactoryApp {

	public static void main(String[] args) {
		Connection conn = ConnectionFactory.getConnection(new SecureConnectionFactory(ConnType.MYSQL) );
		System.out.println(conn.getSecStatus());
		System.out.println(conn instanceof MysqlConnection);
	}
}

class ConnectionFactory {
	
	public static Connection getConnection(IConnectionFactory factory) {
		return factory.getConnection();
	}
}

abstract class Connection{
	protected SecurityStatus secStatus;
	
	public SecurityStatus getSecStatus() {
		return secStatus;
	}

	public Connection(SecurityStatus secStatus) {
		this.secStatus = secStatus;
	}
}
class MysqlConnection extends Connection{
	MysqlConnection(SecurityStatus secStatus){
		super(secStatus);
	}
}
class OracleConnection extends Connection{
	OracleConnection(SecurityStatus secStatus){
		super(secStatus);
	}
}
class SybaseConnection extends Connection{
	SybaseConnection(SecurityStatus secStatus){
		super(secStatus);
	}
}

enum ConnType{
	MYSQL,ORACLE,SYBASE;
}
enum SecurityStatus{
	SECURE,UNSECURE;
}

interface IConnectionFactory{
	Connection getConnection();
	
	default Connection getConnection(ConnType type, SecurityStatus status) {
		switch(type) {
			case MYSQL : 
				return new MysqlConnection(status);
			case ORACLE : 
				return new OracleConnection(status);
			case SYBASE : 
				return new SybaseConnection(status);
			default : 
				throw new RuntimeException("Invalid Connection Type");
		}
	}
}

class SecureConnectionFactory implements IConnectionFactory{
	
	private ConnType type;
	
	SecureConnectionFactory(ConnType type){
		this.type = type;
	}
	
	public Connection getConnection() {
		return getConnection(type, SecurityStatus.SECURE);
	}
}

class UnsecureConnectionFactory implements IConnectionFactory{
	
	private ConnType type;
	
	UnsecureConnectionFactory(ConnType type){
		this.type = type;
	}
	
	public Connection getConnection() {
		return getConnection(type, SecurityStatus.UNSECURE);
	}
}
