package com.sai.patterns.simplefactory;

public class SimpleFactoryApp {
	public static void main(String...args) {
		Computer comp = ComputerFactory.createComputer(CompType.PC);
		System.out.println(comp instanceof PC);
	}
}

class ComputerFactory{
	public static Computer createComputer(CompType type) {
		switch(type) {
		case PC : 
			return new PC();
		case SERVER : 
			return new Server();
		default : 
			throw new RuntimeException("Invalid Comptuer Type");			
		}
	}
}

enum CompType{
	PC,SERVER;
}
class PC extends Computer{
	
}
class Server extends Computer{
	
}

abstract class Computer{
	private String ram;
	private String hdd;
	private String cpu;
	
	public String getRam() {
		return ram;
	}
	public void setRam(String ram) {
		this.ram = ram;
	}
	public String getHdd() {
		return hdd;
	}
	public void setHdd(String hdd) {
		this.hdd = hdd;
	}
	public String getCpu() {
		return cpu;
	}
	public void setCpu(String cpu) {
		this.cpu = cpu;
	}
	@Override
	public String toString() {
		return "Computer [ram=" + ram + ", hdd=" + hdd + ", cpu=" + cpu + "]";
	}
}
