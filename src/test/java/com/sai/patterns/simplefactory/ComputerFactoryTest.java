package com.sai.patterns.simplefactory;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ComputerFactoryTest {

	@Test
	public void createComputer_PCCreated() {
		Computer comp = ComputerFactory.createComputer(CompType.PC);
		assertTrue(comp instanceof PC);
	}
	
	@Test
	public void createComputer_ServerCreated() {
		Computer comp = ComputerFactory.createComputer(CompType.SERVER);
		assertTrue(comp instanceof Server);
	}
	@Test(expected=RuntimeException.class)
	public void createComputer_InvalidType() {
		ComputerFactory.createComputer(null);
	}

}
