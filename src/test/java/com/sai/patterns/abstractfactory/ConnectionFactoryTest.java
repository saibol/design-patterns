package com.sai.patterns.abstractfactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConnectionFactoryTest {

	@Test
	public void getConnection_secureOracleConn() {
		Connection conn = ConnectionFactory.getConnection(new SecureConnectionFactory(ConnType.ORACLE));
		assertTrue(conn instanceof OracleConnection);
		assertEquals(SecurityStatus.SECURE,conn.getSecStatus());
	}
	
	@Test
	public void getConnection_unsecureOracleConn() {
		Connection conn = ConnectionFactory.getConnection(new UnsecureConnectionFactory(ConnType.MYSQL));
		assertTrue(conn instanceof MysqlConnection);
		assertEquals(SecurityStatus.UNSECURE,conn.getSecStatus());
	}
}
